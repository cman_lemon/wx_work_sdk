<?php

namespace WxWorkSDK\SendMessage;

use WxWorkSDK\Result;
use WxWorkSDK\SendMessage\ApplicationMessage\Markdown;
use WxWorkSDK\SendMessage\ApplicationMessage\Text;
use WxWorkSDK\SendMessage\ApplicationMessage\TextCard;
use WxWorkSDK\SendMessage\ChatMessage\CreateChat;
use WxWorkSDK\SendMessage\ChatMessage\GetChat;
use WxWorkSDK\SendMessage\ChatMessage\TextCardChat;
use WxWorkSDK\SendMessage\ChatMessage\TextChat;
use WxWorkSDK\SendMessage\ChatMessage\UpdateChat;

/***
 * Class SendMessage
 * @package    WxWorkSDK\SendMessage
 * @author     George
 * @datetime   2019/5/23 15:08
 * @annotation 消息推送
 */
class SendMessage
{
    /**
     * @var string
     * @annotation
     */
    public $accessToken = '';
    /**
     * @var string
     * @annotation 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
     */
    public $toUser = "";

    /**
     * @var string
     * @annotation 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
     */
    public $toParty = "";
    /**
     * @var string
     * @annotation 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
     */
    public $toTag = "";

    /**
     * @var null
     * @annotation 企业应用的id，整型。
     */
    public $agentId = null;

    /**
     * @var int|mixed
     * @annotation
     */
    public $safe = 0;

    /**
     * @var string
     * @annotation 群聊id
     */
    public $chatId = "";

    /**
     * SendMessage constructor.
     * @param  array  $param
     */
    public function __construct(array $param = [])
    {
        if (isset($param['access_token'])) {
            $this->accessToken = $param['access_token'];
        }
        if (isset($param['touser'])) {
            $this->toUser = $param['touser'];
        }
        if (isset($param['toparty'])) {
            $this->toParty = $param['toparty'];
        }
        if (isset($param['totag'])) {
            $this->toTag = $param['totag'];
        }
        if (isset($param['agentid'])) {
            $this->agentId = $param['agentid'];
        }
        if (isset($param['safe'])) {
            $this->safe = $param['safe'];
        }
        if (isset($param['chatid'])) {
            $this->chatId = $param['chatid'];
        }
    }

    /**
     * @param  string  $content
     * @return Result
     * @annotation
     */
    public function textToApp(string $content)
    {
        $text = new Text($content);
        $text->accessToken = $this->accessToken;
        $text->agentId = $this->agentId;
        $text->toUser = $this->toUser;
        $text->toParty = $this->toParty;
        $text->toTag = $this->toTag;
        $text->safe = $this->safe;
        return $text->send();
    }

    /**
     * @param  string  $title
     * @param  string  $description
     * @param  string  $url
     * @param  string  $btnTxt
     * @return Result
     * @annotation
     */
    public function textCardToAPP(string $title, string $description, string $url, string $btnTxt = '详情')
    {
        $textCard = new TextCard($title, $description, $url, $btnTxt);
        $textCard->accessToken = $this->accessToken;
        $textCard->agentId = $this->agentId;
        $textCard->toUser = $this->toUser;
        $textCard->toParty = $this->toParty;
        $textCard->toTag = $this->toTag;
        $textCard->safe = $this->safe;
        return $textCard->send();
    }

    /**
     * 发送markdown应用消息
     * @param  string  $content
     * @return Result
     */
    public function markdownToApp(string $content): Result
    {
        $msg = new Markdown($content);
        $msg->accessToken = $this->accessToken;
        $msg->agentId = $this->agentId;
        $msg->toUser = $this->toUser;
        $msg->toParty = $this->toParty;
        $msg->toTag = $this->toTag;
        return $msg->send();
    }

    /**
     * @param  array   $userList
     * @param  string  $name
     * @param  string  $owner
     * @param  string  $chatId
     * @return Result
     * @annotation 创建群聊
     */
    public function createChat(array $userList, string $name = '', string $owner = '', string $chatId = '')
    {
        $obj = new CreateChat($this->accessToken, $userList, $name, $owner, $chatId);
        return $obj->create();
    }

    /**
     * @param  string  $chatId
     * @param  string  $name
     * @param  string  $owner
     * @param  array   $addUserList
     * @param  array   $delUserList
     * @return Result
     * @annotation
     */
    public function updateChat(
        string $chatId,
        string $name = '',
        string $owner = '',
        array $addUserList = [],
        array $delUserList = []
    ) {
        $obj = new UpdateChat($this->accessToken, $chatId, $name, $owner, $addUserList, $delUserList);
        return $obj->update();
    }

    /**
     * @return Result
     * @annotation
     */
    public function getChat()
    {
        $obj = new GetChat($this->accessToken, $this->chatId);
        return $obj->get();
    }

    /**
     * @param  string  $content
     * @return Result
     * @annotation
     */
    public function textToChat(string $content)
    {
        $obj = new TextChat($content);
        $obj->accessToken = $this->accessToken;
        $obj->safe = $this->safe;
        $obj->chatId = $this->chatId;
        $result = $obj->send();
        return $result;
    }

    /**
     * @param  string  $title
     * @param  string  $description
     * @param  string  $url
     * @param  string  $btnTxt
     * @return Result
     * @annotation
     */
    public function textCardToChat(string $title, string $description, string $url, string $btnTxt = '详情')
    {
        $obj = new TextCardChat($title, $description, $url, $btnTxt);
        $obj->accessToken = $this->accessToken;
        $obj->safe = $this->safe;
        $obj->chatId = $this->chatId;
        $result = $obj->send();
        return $result;
    }
}