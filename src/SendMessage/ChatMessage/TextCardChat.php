<?php

namespace WxWorkSDK\SendMessage\ChatMessage;

class TextCardChat extends SendBase
{

    /**
     * @var string
     * @annotation 标题，不超过128个字节，超过会自动截断
     */
    private $title;

    /**
     * @var string
     * @annotation 描述，不超过512个字节，超过会自动截断
     */
    private $description;
    /**
     * @var string
     * @annotation 点击后跳转的链接。
     */
    private $url;

    /**
     * @var string
     * @annotation 按钮文字。 默认为“详情”， 不超过4个文字，超过自动截断。
     */
    private $btnTxt;

    /**
     * TextCardChat constructor.
     * @param  string  $title
     * @param  string  $description
     * @param  string  $url
     * @param  string  $btnTxt
     */
    public function __construct(string $title, string $description, string $url, string $btnTxt = '详情')
    {
        $this->title = $title;
        $this->description = $description;
        $this->url = $url;
        $this->btnTxt = $btnTxt;
    }

    /**
     * @return array
     * @annotation
     */
    public function buildParam(): array
    {
        return [
            "chatid"   => $this->chatId,
            "msgtype"  => 'textcard',
            "textcard" => [
                'title'       => $this->title,
                "description" => $this->description,
                "url"         => $this->url,
                "btntxt"      => $this->btnTxt,
            ],
            "safe"     => $this->safe
        ];
    }

    /**
     * @return string
     * @annotation 按照规则自定义人数据检测
     */
    public function customCheck(): string
    {
        if (empty($this->title)) {
            return "无 消息标题";
        }
        if (empty($this->description)) {
            return "无消息描述";
        }
        if (empty($this->url)) {
            return "无点击后跳转的链接";
        }
        return '';
    }
}