<?php

namespace WxWorkSDK\SendMessage\ChatMessage;

class TextChat extends SendBase
{
    private $content;

    /**
     * Text constructor.
     * @param  string  $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     * @annotation
     */
    public function buildParam(): array
    {
        return [
            "chatid"  => $this->chatId,
            "msgtype" => 'text',
            "text"    => [
                'content' => $this->content
            ],
            "safe"    => $this->safe
        ];
    }

    /**
     * @return string
     * @annotation 按照规则自定义人数据检测
     */
    public function customCheck(): string
    {
        if (empty($this->content)) {
            return "无消息内容";
        }
        return '';
    }
}