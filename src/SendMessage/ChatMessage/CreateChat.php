<?php

namespace WxWorkSDK\SendMessage\ChatMessage;

use WxWorkSDK\Error\Error;
use WxWorkSDK\Http\Http;
use WxWorkSDK\Result;

/**
 * Class CreateChat
 * @package  WxWorkSDK\SendMessage\ChatMessage
 * @author   George
 * @datetime 2019/5/28 10:43
 * @annotation
 */
class CreateChat
{
    const URI = 'appchat/create?access_token=';
    /**
     * @var
     * @annotation 群聊名，最多50个utf8字符，超过将截断
     */
    private $name = '';

    /**
     * @var string
     * @annotation 指定群主的id。如果不指定，系统会随机从userlist中选一人作为群主
     */
    private $owner = '';

    /**
     * @var array
     * @annotation 群成员id列表。至少2人，至多500人
     */
    private $userList = [];
    /**
     * @var string
     * @annotation 群聊的唯一标志，不能与已有的群重复；字符串类型，最长32个字符。只允许字符0-9及字母a-zA-Z。如果不填，系统会随机生成群id
     */
    private $chatId = '';

    /**
     * @var string
     * @annotation
     */
    private $token = '';

    /**
     * CreateChat constructor.
     * @param string $token
     * @param array  $userList
     * @param string $name
     * @param string $owner
     * @param string $chatId
     */
    public function __construct(string $token, array $userList, string $name = '', string $owner = '', string $chatId = '')
    {
        $this->token    = $token;
        $this->name     = $name;
        $this->userList = $userList;
        $this->owner    = $owner;
        $this->chatId   = $chatId;
    }

    /**
     * @return Result
     * @annotation
     */
    public function create(): Result
    {
        if ($check = $this->check() != 0) {
            return new Result(FALSE, 1000, $check);
        }
        $postData = [
            'userlist'     => $this->userList
        ];
        if (!empty($this->name)) {
            $postData['name'] = $this->name;
        }
        if (!empty($this->owner)) {
            $postData['owner'] = $this->owner;
        }
        if (!empty($this->chatId)) {
            $postData['chatid'] = $this->chatId;
        }
        list($bool, $response) = Http::request('POST', self::URI . $this->token, ["json" => $postData]);
        if ($bool) {
            return $this->respond($response);
        } else {
            return $response;
        }
    }

    /**
     * @return int|string
     * @annotation
     */
    private function check()
    {
        if (empty($this->token)) {
            return '无access_token';
        }
        if (empty($this->userList)) {
            return '无群成员';
        }
        if (count($this->userList) < 2 || count($this->userList) > 500) {
            return '群成员id列表。至少2人，至多500人。';
        }
        return 0;
    }

    /**
     * @param $response
     * @return Result
     * @annotation
     */
    private function respond($response): Result
    {
        $response = json_decode($response, TRUE);
        if ($response['errcode'] == 0) {
            return new Result(TRUE, 0, 'ok', $response['chatid']);
        } else {
            return new Result(FALSE, Error::WX_MISTAKE, $response['errcode'] . ':' . $response['errmsg']);
        }
    }
}