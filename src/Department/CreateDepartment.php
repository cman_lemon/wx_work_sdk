<?php

namespace WxWorkSDK\Department;

use WxWorkSDK\Error\Error;
use WxWorkSDK\Http\Http;
use WxWorkSDK\Result;

class CreateDepartment
{
    const URI = "department/create";
    /**
     * @var string
     * @annotation
     */
    private $accessToken = '';
    /**
     * @var string
     * @annotation
     */
    private $name = '';
    /**
     * @var null
     * @annotation
     */
    private $parentId = NULL;
    /**
     * @var null
     * @annotation
     */
    private $order = NULL;
    /**
     * @var null
     * @annotation
     */
    private $id = NULL;

    /**
     * CreateDepartment constructor.
     * @param string   $accessToken
     * @param string   $name
     * @param int      $parentId
     * @param int|NULL $order
     * @param int|NULL $id
     */
    public function __construct(string $accessToken, string $name, int $parentId, int $order = NULL, int $id = NULL)
    {
        $this->accessToken = $accessToken;
        $this->name        = $name;
        $this->parentId    = $parentId;
        $this->order       = $order;
        $this->id          = $id;
    }

    /**
     * @return Result
     * @annotation
     */
    public function create()
    {
        if ($check = $this->check() != 0) {
            return new Result(FALSE, Error::PARAM_MISTAKE, $check);
        }
        $postData = [
            'access_token' => $this->accessToken,
            'name'         => $this->name,
            'parentid'     => $this->parentId
        ];
        if (!empty($this->order)) {
            $postData['order'] = $this->order;
        }
        if (!empty($this->id)) {
            $postData['id'] = $this->id;
        }
        list($bool, $response) = Http::request('POST', self::URI, ['query' => $postData]);
        if ($bool) {
            return $this->response($response);
        } else {
            return $response;
        }
    }

    private function check()
    {
        if (empty($this->accessToken)) {
            return '无TOKEN';
        }
        if (empty($this->name)) {
            return '无部门名称';
        }
        if (empty($this->parentId)) {
            return '无部门父级ID';
        }
        return 0;
    }

    /**
     * @param $response
     * @return Result
     * @annotation
     */
    private function response($response): Result
    {
        $response = json_decode($response, TRUE);
        if ($response['errcode'] == 0) {
            return new Result(TRUE, 0, 'ok', $response['id']);
        } else {
            return new Result(FALSE, Error::WX_MISTAKE, $response['errcode'] . ':' . $response['errmsg']);
        }
    }
}