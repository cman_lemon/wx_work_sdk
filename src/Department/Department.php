<?php

namespace WxWorkSDK\Department;

use WxWorkSDK\Result;

class Department
{
    /**
     * @var string
     * @annotation
     */
    public $accessToken = "";

    /**
     * Department constructor.
     * @param string $accessToken
     */
    public function __construct(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @param string   $name
     * @param int      $parentId
     * @param int|NULL $order
     * @param int|NULL $id
     * @return Result
     * @annotation
     */
    public function create(string $name, int $parentId, int $order = NULL, int $id = NULL)
    {
        $obj = new CreateDepartment($this->accessToken, $name, $parentId, $order, $id);
        return $obj->create();
    }

    /**
     * @param string   $name
     * @param int      $parentId
     * @param int|NULL $order
     * @param int|NULL $id
     * @return Result
     * @annotation
     */
    public function update(string $name, int $parentId, int $order = NULL, int $id = NULL)
    {
        $obj = new UpdateDepartment($this->accessToken, $name, $parentId, $order, $id);
        return $obj->update();
    }

    public function delete(string $id)
    {
        $obj = new deleteDepartment($id, $this->accessToken);
        return $obj->delete();
    }

    /**
     * @param string $id
     * @return Result
     * @annotation
     */
    public function list(string $id)
    {
        $obj = new GetDepartment($id, $this->accessToken);
        return $obj->getList();
    }
}