<?php

namespace WxWorkSDK\Member;

use WxWorkSDK\Result;

/**
 * Class Member
 * @package  WxWorkSDK\Member
 * @author   George
 * @datetime 2019/5/30 14:54
 * @annotation
 */
class Member
{
    /**
     * @var string
     * @annotation
     */
    public $accessToken = "";

    public function __construct(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @param int $departmentId
     * @param int $fetchChild
     * @return Result
     * @annotation 获取部门下详细的员工列表
     */
    public function list(int $departmentId, int $fetchChild = 0)
    {
        $obj = new UserList($this->accessToken, $departmentId, $fetchChild);
        return $obj->list();
    }

    /**
     * @param int $departmentId
     * @param int $fetchChild
     * @return Result
     * @annotation 获取部门下简单的员工列表
     */
    public function simpleList(int $departmentId, int $fetchChild = 0)
    {
        $obj = new UserList($this->accessToken, $departmentId, $fetchChild);
        return $obj->simpleList();
    }
}