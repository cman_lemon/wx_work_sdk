<?php

namespace WxWorkSDK\Member;

use Exception;
use WxWorkSDK\Error\Error;
use WxWorkSDK\Http\Http;
use WxWorkSDK\Result;

class User
{
    /**
     * @param  string  $accessToken
     * @param  string  $userId
     * @return Result
     * @throws Exception
     */
    public static function get(string $accessToken, string $userId): Result
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=$accessToken&userid=$userId";
        /**
         * @var string|Result $resInfo
         */
        [$bool, $resInfo] = Http::request('GET', $url);
        if (!$bool) {
            throw new Exception($resInfo->getMsg());
        }
        $response = json_decode($resInfo, true);
        if ($response['errcode'] == 0) {
            return new Result(true, 0, 'ok', $response);
        } else {
            return new Result(false, Error::WX_MISTAKE, $response['errcode'].':'.$response['errmsg']);
        }
    }
}