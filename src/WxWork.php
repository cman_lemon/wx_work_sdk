<?php


namespace WxWorkSDK;

use WxWorkSDK\Authorization\Authorization;
use WxWorkSDK\Department\Department;
use WxWorkSDK\JS\ApiTicket;
use WxWorkSDK\Member\Member;
use WxWorkSDK\SendMessage\SendMessage;

/**
 * Class WxWork
 * @package  WxWorkSDK
 * @author   George
 * @datetime 2019/5/23 14:29
 */
class WxWork
{
    public static function authorization($corpId, $secret)
    {
        return new Authorization($corpId, $secret);
    }

    public static function sendMessage(array $param = [])
    {
        return new SendMessage($param);
    }

    public static function member(string $token)
    {
        return new Member($token);
    }

    public static function department(string $token)
    {
        return new Department($token);
    }

    /**
     * @param  string  $token
     * @return ApiTicket
     */
    public static function jsApiTicket(string $token)
    {
        return new ApiTicket($token);
    }

    /**
     * 群聊机器人消息
     * @param  string  $robotKey
     * @return GroupChatRobot\GroupChatRobot
     */
    public static function groupChatRobot(string $robotKey)
    {
        return new GroupChatRobot\GroupChatRobot($robotKey);
    }
}