<?php

namespace WxWorkSDK\Error;
/**
 * Class Error
 * @package  WxWorkSDK\Error
 * @author   George
 * @datetime 2019/5/25 11:27
 * @annotation
 */
class Error
{
    const PARAM_MISTAKE   = 1000;
    const WX_MISTAKE      = 2000;
    const NET_MISTAKE     = 3000;
    const UNKNOWN_MISTAKE = 4000;

    const NOT_CORP_ID_MSG     = "无企业的id";
    const NOT_AGENT_ID_MSG    = "无企业应用的id";
    const NOT_SECRET_MSG      = "无秘钥";
    const NOT_USER_MSG        = "无消息接收者";
    const NOT_TOKEN_MSG       = "无TOKEN";
    const UNKNOWN_MISTAKE_MSG = "unknown mistake";
}