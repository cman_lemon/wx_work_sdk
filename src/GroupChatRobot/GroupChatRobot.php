<?php

namespace WxWorkSDK\GroupChatRobot;

use WxWorkSDK\GroupChatRobot\Msg\Markdown;
use WxWorkSDK\GroupChatRobot\Msg\Text;

/**
 * Class GroupChatRobot
 * @package WxWorkSDK\GroupChatRobot
 */
class GroupChatRobot
{
    /**
     * @var string $robotKey
     */
    protected $robotKey;

    /**
     * GroupChatRobot constructor.
     * @param  string  $robotKey
     */
    public function __construct(string $robotKey)
    {
        $this->robotKey = $robotKey;
    }

    /**
     * 文本消息
     * @return Text
     */
    public function textMsg(): Text
    {
        return new Text($this->robotKey);
    }

    /**
     * markdown消息
     * @return Markdown
     */
    public function markDownMsg(): Markdown
    {
        return new Markdown($this->robotKey);
    }
}