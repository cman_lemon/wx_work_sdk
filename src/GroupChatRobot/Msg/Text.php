<?php

namespace WxWorkSDK\GroupChatRobot\Msg;

/**
 * Class Text
 * @package WxWorkSDK\GroupChatRobot\Types
 */
class Text extends Msg
{
    private string $msgType               = 'text';
    private string $content               = '';
    private array  $mentioned_list        = [];
    private array  $mentioned_mobile_list = [];

    /**
     * @param  string  $content
     * @return Text
     */
    public function setContent(string $content): Text
    {
        $this->content = substr($content, 0, 2048);
        return $this;
    }

    /**
     * @param  array  $list
     * @return Text
     */
    public function setMentionedList(array $list): Text
    {
        $this->mentioned_list = $list;
        return $this;
    }

    /**
     * @param  array  $list
     * @return Text
     */
    public function setMentionedMobileList(array $list): Text
    {
        $this->mentioned_mobile_list = $list;
        return $this;
    }

    /**
     * @param  string  $userId
     * @return $this
     */
    public function atUserId(string $userId): Text
    {
        $this->mentioned_list[] = $userId;
        return $this;
    }

    /**
     * @return $this
     */
    public function atAll(): Text
    {
        $this->mentioned_list[] = '@all';
        return $this;
    }

    /**
     * @param  string  $mobile
     * @return $this
     */
    public function atMobile(string $mobile): Text
    {
        $this->mentioned_mobile_list[] = $mobile;
        return $this;
    }

    /**
     * @return array
     */
    public function msgBody(): array
    {
        $body = [
            'content' => $this->content,
        ];
        if ($this->mentioned_list) {
            $body['mentioned_list'] = $this->mentioned_list;
        }
        if ($this->mentioned_mobile_list) {
            $body['mentioned_mobile_list'] = $this->mentioned_mobile_list;
        }
        return [
            'msgtype' => $this->msgType,
            'text'    => $body,
        ];
    }
}