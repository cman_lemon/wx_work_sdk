<?php
declare(strict_types=1);

namespace WxWorkSDK\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use WxWorkSDK\Error\Error;
use WxWorkSDK\Result;

/**
 * Class Http
 * @package  WxWorkSDK\Http
 * @author   George
 * @datetime 2019/5/25 11:27
 * @annotation
 */
class Http
{
    const BASE_URL = "https://qyapi.weixin.qq.com/cgi-bin/";

    public static function request(string $method, string $uri, array $param = [])
    {
        $client = new Client([
            'base_uri' => self::BASE_URL
        ]);
        try {
            $response = $client->request($method, $uri, $param);
            return [true, $response->getBody()->getContents()];
        } catch (GuzzleException $e) {
            return [false, new Result(false, Error::NET_MISTAKE, $e->getMessage())];
        }
    }
}