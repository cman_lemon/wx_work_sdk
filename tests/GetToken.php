<?php
require __DIR__ . "/../vendor/autoload.php";
require __DIR__ . "/../config.php";

use PHPUnit\Framework\TestCase;
use WxWorkSDK\Result;
use WxWorkSDK\WxWork;

class GetToken extends TestCase
{
    public function testGetToken()
    {
        $this->assertInstanceOf(Result::class, WxWork::authorization(CORP_ID, SECRET)->getToken());
    }
}