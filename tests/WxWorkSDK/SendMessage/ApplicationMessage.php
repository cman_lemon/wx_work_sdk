<?php

namespace WxWorkSDK\SendMessage;

require __DIR__."/../../../vendor/autoload.php";
require __DIR__."/../../../config.php";

use PHPUnit\Framework\TestCase;

class ApplicationMessage extends TestCase
{
    /**
     *
     */
    public function testText()
    {
        $sendMsg = new SendMessage();
        $sendMsg->accessToken = AccessToken;
        $sendMsg->agentId = AgentId;
        $sendMsg->toUser = 'stlswm';
        $r = $sendMsg->textToApp('test');
        $this->assertEquals(0, $r->getCode());
    }

    public function testTextCard()
    {
        $sendMsg = new SendMessage();
        $sendMsg->accessToken = AccessToken;
        $sendMsg->agentId = AgentId;
        $sendMsg->toUser = 'stlswm';
        $r = $sendMsg->textCardToAPP('123', 'abc', 'https://www.baidu.com');
        $this->assertEquals(0, $r->getCode());
    }

    /**
     *
     */
    public function testMarkdown()
    {
        $sendMsg = new SendMessage();
        $sendMsg->accessToken = AccessToken;
        $sendMsg->agentId = AgentId;
        $sendMsg->toUser = 'stlswm';
        $r = $sendMsg->markdownToApp('test');
        $this->assertEquals(0, $r->getCode());
    }
}